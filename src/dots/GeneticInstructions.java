
/*
 * File: GeneticInstructions.java
 * Author: Nick Horn
 * Date Created: 5-15-18
 * Date Updated: 5-26-18
 * 
 * Description: Part of the Genetic Algorithm.
 * 		This is the 'genetic' data which determines
 * 		the behavior of it's containing object (Dot).
 * 		Implements methods needed for the algorithm.
 * 
 * ToDo: Replace this implementation with NEAT.
 * 
 */

package dots;

import java.util.Random;
import javafx.geometry.Point2D;

public class GeneticInstructions {

	private static final int INSTRUCTION_ARRAY_SIZE = 500; //500 is stable with 2k dots, don't go too high unless a map is complex enough
														   //to require it.  Going to low can cause problems if the dots waste moves early.
	private static final double MUTATION_RATE = 0.01; //too much mutation is a bad thing, but not enough leads to longer runs
													  // one percent (0.01) recommended.
	
	private Point2D[] instructions;
	private int step = 0; //number of instructions executed
	
	public GeneticInstructions() {
		instructions= new Point2D[INSTRUCTION_ARRAY_SIZE];
		for(int i = 0; i < instructions.length; i++) {
			instructions[i] = new Point2D(0, 0);
		}
		randomize();
	}//end constructor
	
	public Point2D[] getInstructions() {
		return instructions;
	}
	
	public void setInstrucitons(Point2D[] newInstr) {
		instructions = newInstr;
	}
	
	public Point2D getCurrentInstruction() {
		return instructions[step];
	}
	
	
	public int getInstructionListLength() {
		return instructions.length;
	}
	
	public int getStep() {
		return step;
	}
	
	public void incrementStep() {
		step++;
	}
	
	//randomly generates x and y for each point in the instructions
	private void randomize() {
		Random rand = new Random();
		double x, y;
		double negativeCheck;
		for(int i = 0; i < instructions.length; i++) {
			x = rand.nextDouble() * 2 * Math.PI;
			y = rand.nextDouble() * 2 * Math.PI;
			
			negativeCheck = rand.nextDouble();
			if(negativeCheck >= 0.5) {
				x *= -1;
			}
			negativeCheck = rand.nextDouble();
			if(negativeCheck >= 0.5) {
				y *= -1;
			}
			
			instructions[i] = instructions[i].add(x, y);
		}
	}//end randomize
	
	@Override
	public GeneticInstructions clone() throws CloneNotSupportedException {
		super.clone();
		GeneticInstructions clone = new GeneticInstructions();
		Point2D[] cloneInstructions= new Point2D[instructions.length];
		for(int i = 0; i < instructions.length; i++) {
			double newX, newY;
			newX = instructions[i].getX();
			newY = instructions[i].getY();
			cloneInstructions[i] = new Point2D(newX, newY);
		}
		clone.setInstrucitons(cloneInstructions);
		return clone;
	}//end clone
	
	//low chance to change any instruction
	public void mutate() {
		Random rand = new Random();
		double mutateCheck;
		double negativeCheck;
		double x, y;
		for(int i = 0; i < instructions.length; i++) {
			mutateCheck = Math.random();
			if(mutateCheck <= MUTATION_RATE) { //mutate this instruction
				x = rand.nextDouble() * 2 * Math.PI;
				y = rand.nextDouble() * 2 * Math.PI;
				
				negativeCheck = rand.nextDouble();
				if(negativeCheck >= 0.5) {
					x *= -1;
				}
				negativeCheck = rand.nextDouble();
				if(negativeCheck >= 0.5) {
					y *= -1;
				}
				
				instructions[i] = new Point2D(x, y);
			}
		}//end loop
	}//end mutate
	
}//end class