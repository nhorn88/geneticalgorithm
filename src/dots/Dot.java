
/*
 * File: Dot.java
 * Author: Nick Horn
 * Date Created: 5-15-18
 * Date Updated: 5-26-18
 * 
 * Description: Part of the Genetic Algorithm.
 * 		This is the 'species' and holds position
 * 		data and a genetic component: GeneticInstructons.
 * 		Implements methods needed for the algorithm.
 * 
 */

package dots;

import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.shape.Rectangle;

public class Dot {

	private static double MAX_VEL = 5; //controls max step size, higher numbers mean "faster"(farther) moving dots 
									   //however, moving faster may allow dots to jump through thin obstacles or over corners.
									   //5 has been reasonable for maps so far and dots at 500 instructions.
									   //faster dots generally require less instructions and complete more complex maps in less time.
	
	private Point2D pos;
	private Point2D vel;
	private Point2D acc;
	private ObservableList<Node> obstacles;
	private Point2D goal;
	
	private GeneticInstructions brain;
	
	private boolean dead = false; //dead when it hits an obstacle or wall, 
								  //reaches the goal or runs out of instructions
	private boolean reachedGoal = false;
	private boolean champion = false; //champion is best of it's generation
	
	private float fitness = 0; //fitness is based on distance to goal and is used in 
							   //selection process for the next generation
	private double windowH, windowW;
	
	public Dot(double h, double w, Point2D goal, ObservableList<Node> obstacles) {
		windowH = h;
		windowW = w;
		brain = new GeneticInstructions();
		pos = new Point2D(windowW/2, windowH-100);
		vel = new Point2D(0, 0);
		acc = new Point2D(0, 0);
		this.obstacles = obstacles;
		this.goal = goal;
	}//end constructor
	
	public boolean isChampion() {
		return champion;
	}
	
	public void setChampion(boolean flag) {
		champion = flag;
	}
	
	public Point2D[] getInstructions() {
		return brain.getInstructions();
	}
	
	public void setInstructions(Point2D[] newInstr) {
		brain.setInstrucitons(newInstr);
	}
	
	public Point2D getPos() {
		return pos;
	}//end getPos
	
	public boolean isDead() {
		return dead;
	}
	
	public void kill() {
		dead = true;
	}
	
	public boolean reachedGoal() {
		return reachedGoal;
	}
	
	public void setReachedGoal() {
		reachedGoal = true;
	}
	
	public float getFitness() {
		return fitness;
	}
	
	public int getStep() {
		return brain.getStep();
	}
	
	public void normalizeFitness(float fitnessSum) {
		fitness /= fitnessSum;
	}
	
	//get an instruction from brain and move
	public void move() {
		if(brain.getStep() < brain.getInstructionListLength()) { //instructions left
			acc = brain.getCurrentInstruction();
			brain.incrementStep();
		} else { //ran out of instructions, dot is dead
			kill();
		}
		
		//apply acceleration
		vel = vel.add(acc);
		limitVel(); //make sure not too fast
		pos = pos.add(vel);
	}//end move
	
	//make sure velocity stays in a range of +/-5
	private void limitVel() {
		double x = vel.getX();
		double y = vel.getY();
		boolean velChanged = false;
		if(x > MAX_VEL) {
			x = MAX_VEL;
			velChanged = true;
		}
		if(y > MAX_VEL) {
			y = MAX_VEL;
			velChanged = true;
		}
		if(x < -MAX_VEL) {
			x = -MAX_VEL;
			velChanged = true;
		}
		if(y < -MAX_VEL) {
			y = -MAX_VEL;
			velChanged = true;
		}
		if(velChanged) {
			vel = new Point2D(x, y);
		}
	}//end limitVel
	
	//try moving and check collisions etc.
	public void update() {
		if(dead == false && reachedGoal == false) {
			move();

			if(pos.getX() >= windowW-2.75 || pos.getX() <= 2.75 || pos.getY() >= windowH-34.5 || pos.getY() <= 3) {
				//too close to wall
				kill();
			}
			
			Rectangle temp;
			for(Node obst:obstacles) {
				temp = (Rectangle)obst;
				if(pos.getX() > (temp.getX() - 2.75) && pos.getX() < (temp.getX() + temp.getWidth() + 2.75) 
				&& pos.getY() > (temp.getY() - 2.75) && pos.getY() < (temp.getY() + temp.getHeight() + 2.75)) {
					//inside of an obstacle
					kill();
				}
			}
			
			if(pos.distance(goal) <= 4) { //goal reached
				setReachedGoal();
			}
			
		}//end not dead/goal
	}//end update
	
	public void calculateFitness() {
		if(reachedGoal) {
			fitness = 1.0f/16.0f + 10000000.0f/(float)(brain.getStep() * brain.getStep());
		}
		else {
			float distanceToGoal = (float)pos.distance(goal);
			fitness = 1.0f/(distanceToGoal * distanceToGoal);
		}
	}//end calculate fitness
	
	public Dot reproduce() {
		Dot baby = new Dot(windowH, windowW, goal, obstacles);
		baby.setInstructions(this.getInstructions().clone());
		return baby;
	}//end reproduce
	
	public void mutate() {
		brain.mutate();
	}//end mutate
	
}//end class