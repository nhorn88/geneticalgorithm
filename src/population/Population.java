
/*
 * File: Population.java
 * Author: Nick Horn
 * Date Created: 5-15-18
 * Date Updated: 5-26-18
 * 
 * Description: Part of the Genetic Algorithm.
 * 		Holds a generation of Dots and implements
 * 		methods needed for the algorithm.
 * 
 */

package population;

import java.util.Random;
import dots.Dot;
import javafx.collections.ObservableList;
import javafx.geometry.Point2D;
import javafx.scene.Node;

public class Population {

	private static final int POPULATION_SIZE = 2000; //assuming INSTRUCTION_ARRAY_SIZE = 500 in GeneticInstructions:
												     //2k is stable, 2.5k runs but struggles, 3k is too much!
	private static final int TOURNAMENT_SIZE = POPULATION_SIZE / 30; //larger tournaments favor stronger dots. 6+ offers a little diversity
	private static final double ELITEISM_FACTOR = 0.05; //percent of population based directly on champion dot, keep within [0.01-1] to be safe
	
	private Dot[] dots;
	private float fitnessSum =0;
	private int generation = 1;
	private int championIndex = 0;
	private int lowestSteps = 9000;
	
	public Population(double h, double w, Point2D goal, ObservableList<Node> obstacles) {
		dots = new Dot[POPULATION_SIZE];
		for(int i = 0; i < dots.length; i++) {
			dots[i] = new Dot(h, w, goal, obstacles);
		}
	}//end constructor
	
	public int getGeneration() {
		return generation;
	}
	
	public int getLowestSteps() {
		return lowestSteps;
	}
	
	public int getPopulationSize() {
		return POPULATION_SIZE;
	}
	
	public Point2D[] getAllDotPos() {
		Point2D[] result = new Point2D[dots.length];
		for(int i = 0; i < dots.length; i++) {
			result[i] = dots[i].getPos();
		}
		return result;
	}//end getAllDotPos
	
	public boolean isDotChampion(int index) {
		return dots[index].isChampion();
	}//end isDotChampion
	
	//update all the dots
	public void update() {
		for(int i = 0; i < dots.length; i++) {
			if(dots[i].getStep() > lowestSteps) { //this dot is worse than best dot and can die
				dots[i].kill();
			} else {
				dots[i].update();
			}
		}//end loop
	}//end update
	
	public boolean allDotsDead() {
		for(int i = 0; i < dots.length; i++) {
			if(dots[i].isDead() == false && dots[i].reachedGoal() == false) {
				return false;
			}
		}//end loop
		return true;
	}//end allDotsDead
	
	public void calculateFitness() {
		for(int i = 0; i < dots.length; i++) {
			dots[i].calculateFitness();
		}
	}//end calculateFitness
	
	public void calculateFitnessSum() {
		fitnessSum = 0;
		for(int i = 0; i < dots.length; i++) {
			fitnessSum += dots[i].getFitness();
		}
		
		//normalize
		for(int i = 0; i < dots.length; i++) {
			dots[i].normalizeFitness(fitnessSum);
		}
		fitnessSum = 1;
	}//end calculateFitnessSum
	
	//pseudo-randomly select a dot from current generation through
	//tournament selection.  better fitness increases odds of being selected.
	public Dot selectParent() {
		int size = TOURNAMENT_SIZE;
		
		Random rand = new Random();
		Dot[] brackets = new Dot[size];
		//load with random dots from this generation
		for(int i = 0; i < brackets.length; i++) {
			brackets[i] = dots[rand.nextInt(POPULATION_SIZE)];
		}
		
		//find the most fit dot in tournament. dont have to actually face off since
		//calculations are simple
		int winnerIndex = 0;
		for(int i = 1; i < brackets.length; i++) {
			if(brackets[i].getFitness() > brackets[winnerIndex].getFitness()) {
				winnerIndex = i;
			}
		}
		
		return brackets[winnerIndex];
	}//end selectParent
	
	public void mutateBabies() {
		for(int i = 1; i < dots.length; i++) {
			dots[i].mutate();
		}
	}//end mutateBabies
	
	public void setChampion() {
		float maxFitness = 0;
		int maxIndex = 0;
		
		for(int i = 0; i < dots.length; i++) {
			if(dots[i].getFitness() > maxFitness ) { 
				maxFitness = dots[i].getFitness();
				maxIndex = i;
			}
		}
			
		championIndex = maxIndex;
		
		if(dots[championIndex].reachedGoal()) {
			lowestSteps = dots[championIndex].getStep();
		}
	}//end setBestDot
	
	public void naturalSelection() {
		Dot[] newGen = new Dot[POPULATION_SIZE];
		setChampion();
		calculateFitnessSum();
		
		//champ is guaranteed at least x babies, only the first is mutation free
		for(int i = 0; i < (int)(POPULATION_SIZE * ELITEISM_FACTOR); i++) {
			if(i < POPULATION_SIZE) { //just in case eliteism factor > 1
				newGen[i] = dots[championIndex].reproduce();
			}
		}
		
		dots[championIndex].setChampion(false);
		newGen[0].setChampion(true);
		
		//fill the rest of the generation from pseudo-random parents
		//survival of the most fit (mostly)
		for(int i = (int)(POPULATION_SIZE * ELITEISM_FACTOR); i < newGen.length; i++) {
			if(i < POPULATION_SIZE) { //just in case eliteism factor > 1
				Dot parent = selectParent();
				newGen[i] = parent.reproduce();
			}
		}
		dots = null;
		dots = newGen;
		newGen = null;
		generation++;
	}//end naturalSelection
	
}//end class