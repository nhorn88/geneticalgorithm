
/*
 * File: ComponentSet.java
 * Author: Nick Horn
 * Date Created: 5-16-18
 * Date Updated: 6-2-18
 * 
 * Description: Handles all the GUI logic for the program.
 * 		Class is initialized by the FXML file loaded by DriverGA.
 * 		Contains an instance of ControlsController.java as a nested
 * 		controller, initialized by the main fxml file loaded.
 * 
 */

package mazeComponents;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import population.Population;

public class ComponentSet {
	
	public static final double CIRCLE_RADIUS = 3; //don't change this without adjusting collisions for in the Dot class
	
	//Injected Variables
	@FXML private Pane rootPane; 
	@FXML private Pane mazePane;
	@FXML private GridPane controlPane; //included element of main fxml
	@FXML private ControlsController controlPaneController; //nested controller
	@FXML private Circle goal;
	@FXML private Group obstacleGroup; //container for obstacles
	@FXML private Group dotGroup; //container for dots
	@FXML private ColumnConstraints column0;
	@FXML private ColumnConstraints column1;
	@FXML private RowConstraints row0;
	
	//Other Class Variables
	private Point2D goalPoint;
	private ObservableList<Node> stats;
	private ObservableList<Node> obstacles;
	private Object[] dotCircleArray;
	private Population pop;
	private Timeline timeline;
	private double height;
	private double width;
	private double time = 25; //default durration of timeline's keyframe
	private boolean isPlaying = false; 
	private Point2D[] currentDots;
	private KeyFrame newKeyFrame;
	
	//references from ControlsController set in setUp
	private Group statsGroup; //container for step and gen labels
	private Button playPauseButton; //plays or pauses timeline (toggle)
	private Slider speedSlider; //adjusts duration of timeline's key frame
	
	//set up all variables including Timeline and slider listener
	public void setUp(double h, double w) {
		height = h;
		width = w;
		
		//set up nested controller
		controlPaneController.setReference(this);
		statsGroup = controlPaneController.getStatsGroup();
		playPauseButton = controlPaneController.getPlayPauseButton();
		speedSlider = controlPaneController.getSpeedSlider();
		
		rootPane.setPrefHeight(height);
		rootPane.setPrefWidth(width+300);
		controlPane.setPrefHeight(height);
		column0.setMinWidth(width);
		column0.setMaxWidth(width);
		column1.setMinWidth(300);
		column1.setMaxWidth(300);
		row0.setMinHeight(height);
		stats = statsGroup.getChildren();
		goalPoint = new Point2D(goal.getCenterX(), goal.getCenterY());
		obstacles = obstacleGroup.getChildren();
		pop = new Population(height, width, goalPoint, obstacles); 
		
		//set up circles
		currentDots = pop.getAllDotPos();
		for(int i = 0; i< pop.getPopulationSize(); i++) {
			dotGroup.getChildren().add(new Circle(currentDots[i].getX(), currentDots[i].getY(), CIRCLE_RADIUS, Color.BLACK));
		}
		dotCircleArray = dotGroup.getChildren().toArray();
		
		speedSlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                Number old_val, Number new_val) {
                	sliderChanged(new_val);
            }
        });
		
		timeline = new Timeline(createKeyFrame());
		timeline.setCycleCount(Timeline.INDEFINITE);
	}//end setUp
	
	//create and return a KeyFrame with the current value of TIME
	public KeyFrame createKeyFrame() {
		KeyFrame newKey = new KeyFrame(Duration.millis(time), new EventHandler<ActionEvent>() {
			@Override 
			public void handle(ActionEvent t) {
				displayStats();
				currentDots = pop.getAllDotPos();
				if(pop.allDotsDead() == false) {
					
					drawDots(currentDots);
					pop.update();
				}
				else { //gen finished, create next gen
					pop.calculateFitness();
					pop.naturalSelection();
					pop.mutateBabies(); 
				}
			}
		});
		return newKey;
	}//end createKeyFrame
	
	//alters the durration of timeline's keyframe
	public void sliderChanged(Number newSpeed) {
		time = 60-(double)newSpeed; //max on slider is 50, this offsets so min durration is 10 (not too fast)
		newKeyFrame = createKeyFrame();
		timeline.stop(); //must stop before changing keyframes
		ObservableList<KeyFrame> keyList = timeline.getKeyFrames();
		keyList.clear(); //get rid of the old keyframe
		keyList.add(newKeyFrame);
		if(isPlaying) { //if was previously playing
			timeline.play();
		}
	}//end sliderChanged
	
	//toggle play/pause state of timeline in response to playPauseButton
	public void playPausePressed() {
		if(isPlaying == false) {
			timeline.play();
			playPauseButton.setText("Pause");
			isPlaying = true;
		}
		else{
			timeline.pause();
			playPauseButton.setText("Play");
			isPlaying = false;
		}
	}//end playPausePressed
	
	//updates the circles based on current dots
	public void drawDots(Point2D[] dots) {
		Circle c;
		for(int i = 0; i < dots.length-1; i++) {
			c = (Circle)dotCircleArray[i];
			c.setCenterX(dots[i].getX());
			c.setCenterY(dots[i].getY());
			c.setRadius(CIRCLE_RADIUS);
			c.setFill(Color.BLACK);
		}
		
		
		//champ handled last
		c = (Circle)dotCircleArray[dots.length-1];
		if(pop.isDotChampion(0)) {
			c.setCenterX(dots[0].getX());
			c.setCenterY(dots[0].getY());
			c.setRadius(CIRCLE_RADIUS+1);
			c.setFill(Color.RED);
		}
		else {
			c.setCenterX(dots[0].getX());
			c.setCenterY(dots[0].getY());
			c.setRadius(CIRCLE_RADIUS);
			c.setFill(Color.BLACK);
		}
		
		
	}//drawDots
	
	//clears current stats and displays up to date stats
	public void displayStats() {
		stats.clear();
		Label statsLabel = new Label("Lowest Steps: " + pop.getLowestSteps()
								 + "\nGen: " + pop.getGeneration());
		stats.add(statsLabel);
	}//end displayStats
	
}//end class