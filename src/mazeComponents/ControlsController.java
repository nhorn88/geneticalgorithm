
/*
 * File: ControlsController.java
 * Author: Nick Horn
 * Date Created: 6-2-18
 * Date Updated: 6-2-18
 * 
 * Description: Controller for MazeControls.fxml. This controller 
 * 		is nested within ComponentSet.java as MazeControls is used 
 * 		as an include in maze maps.  This controller serves to provide 
 * 		references to the nested elements and pass event calls back to 
 * 		ComponentSet which handles the main logic of the program.
 * 
 */

package mazeComponents;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;

public class ControlsController {
	
	@FXML private Group statsGroup; //container for step and gen labels
	@FXML private Button playPauseButton; //plays or pauses timeline (toggle)
	@FXML private Slider speedSlider; //adjusts duration of timeline's key frame
	
	private ComponentSet mainController; //reference to the main controller
	
	public void setReference(ComponentSet thisSet) {
		mainController = thisSet;
	}//end setup
	
	public Group getStatsGroup() {
		return statsGroup;
	}
	
	public Button getPlayPauseButton() {
		return playPauseButton;
	}
	
	public Slider getSpeedSlider() {
		return speedSlider;
	}
	
	//toggle play/pause state of timeline in response to playPauseButton
	public void playPausePressed(ActionEvent event) {
		mainController.playPausePressed();
	}//end playPausePressed
	
	
}//end class
