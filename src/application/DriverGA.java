
/*
 * File: DriverGA.java
 * Author: Nick Horn
 * Date Created: 5-15-18
 * Date Updated: 5-26-18
 * 
 * Description: Main class for a JavaFX based genetic algorithm.
 * 		Population consists of Dot objects which evolve over many
 * 		generations to find an "optimal" path to the goal. Parent
 * 		selection does not use crossover, and parents are selected
 * 		via a tournament selection algorithm.
 * 
 * 		**Path is not necessarily truly optimal, but is eventually
 * 		optimized to take the least number of steps possible for the
 * 		path it has (though another shorter path may exist)
 * 
 * ToDo: Replace the brains of the dots with a NEAT implementation
 * 		 of a Neural Network.  A NEAT NN would solve the above
 * 		 optimal path problem by allowing for actual decision making.
 * 		 Currently considering Encog Library.
 * 
 */

package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import mazeComponents.ComponentSet;

public class DriverGA extends Application {

	public static final double HEIGHT = 800; //changing these values may
	public static final double WIDTH = 800;  //cause obstacles to be drawn
											 //differently in relation to dots
	
	/*
	 * ToDo: add functionality to change maps through a dropdown in MazeControls.fxml
	 * 		 and ControlsController.java
	 * 
	 * 		 For now maps must be changed manually in the code by un-commenting the
	 * 		 desired fxmlName declaration below and commenting out the others each run.
	 */
	
	//private String fxmlName = "../mazeComponents/MazeBaseTemplate.fxml";
	private String fxmlName = "../mazeComponents/MazeOne.fxml";
	//private String fxmlName = "../mazeComponents/MazeTwo.fxml";
	//private String fxmlName = "../mazeComponents/MazeThree.fxml";
	//private String fxmlName = "../mazeComponents/MazeBlockGrid.fxml";
	
	private FXMLLoader fxmlLoader;
	private Pane root;
	private ComponentSet components;
	
	public static void main(String[] args) {
		Application.launch(DriverGA.class, args);
	}//end main

	@Override
	public void start(Stage stage) throws Exception {
		fxmlLoader = new FXMLLoader(getClass().getResource(fxmlName));
		root = fxmlLoader.load();
		components = (ComponentSet)fxmlLoader.getController();
		
		stage.setTitle("Dot Maze -- Genetic Algorithm");
		stage.setScene(new Scene(root, WIDTH+300, HEIGHT)); //can add two more fields to set default height/width
		stage.setResizable(false);
		stage.show();
		
		components.setUp(HEIGHT, WIDTH);
	}//end start
	
}//end class